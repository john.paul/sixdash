import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleRoot } from 'radium'

import SDHero from './components/SDHero'
import SDHeading from './components/SDHeading'
import SDSubHeading from './components/SDSubHeading'
import SDText from './components/SDText'
import SDCallToAction from './components/SDCallToAction'
import SDPanel from './components/SDPanel'
import SDPanelSection from './components/SDPanelSection'
import SDPanelHeading from './components/SDPanelHeading'
import SDCard from './components/SDCard'
import SDCardHeader from './components/SDCardHeader'
import SDCardBody from './components/SDCardBody'

const COLORS = [
    '#ff4b42', '#0088FE', '#00C49F',
    '#1daae8', '#FFBB28', '#FF8042',
    '#00c143', '#B7E65A', '#EBEBEB',
    '#212121'
];

class SixDash extends Component {
    render() {
        const { children } = this.props
        let styles = {}
        styles = Object.assign(styles, this.props.style)
        return (
            <StyleRoot>
                <div style={styles}>
                    {children}
                </div>
            </StyleRoot>
        )
    }
}

SixDash.propTypes = {
    style: PropTypes.object
}

SixDash.defaultProps = {
    style: null
}

export { SixDash, SDHero, SDHeading, SDSubHeading, SDText, SDCallToAction, SDPanel, SDPanelSection, SDPanelHeading, SDCard, SDCardHeader, SDCardBody, COLORS }

