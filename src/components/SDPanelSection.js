import React, { Component } from 'react'
import PropTypes from 'prop-types'
import SDPanelHeading from './SDPanelHeading'

export default class SDPanelSection extends Component {
    render() {
        const { children, heading, position } = this.props
        let styles = {
            textAlign: 'center'
        }

        styles = Object.assign(styles, this.props.style)
        return (
            <div style={styles}>
                {position === 'top' && heading}
                {children}
                {position === 'bottom' && heading}
            </div>
        )
    }
}

SDPanelSection.propTypes = {
    style: PropTypes.object,
    position: PropTypes.oneOf(['top', 'bottom']),
    heading: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string,
        PropTypes.object,
        PropTypes.instanceOf(SDPanelHeading)
    ])
}

SDPanelSection.defaultProps = {
    style: {},
    position: 'top'
}
