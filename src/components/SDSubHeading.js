import React,{ Component } from 'react'
import PropTypes from 'prop-types'

export default class SDSubHeading extends Component {
    render() {
        let styles = {
            color: '#F2A560',
            fontWeight: 400,
            fontSize: 24,
            marginTop: 10,
            marginBottom: 5,
            textAlign: 'center'
        }

        styles = Object.assign(styles, this.props.style)
        return (
            <h3 style={styles}>{this.props.children}</h3>
        )
    }
}

SDSubHeading.propTypes = {
    style: PropTypes.object
}

SDSubHeading.defaultProps = {
    style: {}
}
