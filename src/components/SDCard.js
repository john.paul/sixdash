import React, { Component, Children, cloneElement } from 'react'
import PropTypes from 'prop-types'
import Radium from 'radium'

class SDCard extends Component {
    render() {
        const { width, height, children } = this.props
        let styles = {
            marginTop: 20,
            marginBottom: 20
        }

        styles = Object.assign(styles, this.props.style)
        if (width) {
            styles = Object.assign(styles, {width})
        }
        if (height) {
            styles = Object.assign(styles, {height})
        }

        return (
            <div style={styles}>
                {Children.map(children, (c, i) => (
                    cloneElement(c, {
                        styles: c.props.styles
                    })
                ))}
            </div>
        )
    }
}

export default SDCard

SDCard.propTypes = {
    style: PropTypes.object,
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

SDCard.defaultProps = {
    style: {}
}
