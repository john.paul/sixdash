import React from 'react'
import SDSubHeading from './SDSubHeading'
import PropTypes from 'prop-types'

export default class SDPanelHeading extends SDSubHeading {
    render() {
        let styles = {
            color: '#444',
            fontWeight: 400,
            fontSize: 24,
            marginTop: 10,
            marginBottom: 5
        }

        styles = Object.assign(styles, this.props.style)
        return (
            <h3 style={styles}>{this.props.children}</h3>
        )
    }
}

SDPanelHeading.propTypes = {
    style: PropTypes.object
}

SDPanelHeading.defaultProps = {
    style: {}
}
