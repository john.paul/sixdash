import React,{ Component } from 'react'
import PropTypes from 'prop-types'

export default class SDHeading extends Component {
    render() {
        let styles = {
            color: '#F2A560',
            fontWeight: 400,
            fontSize: 38,
            marginTop: 15,
            marginButton: 10,
            textAlign: 'center'
        }

        styles = Object.assign(styles, this.props.style)
        return (
            <h1 style={styles}>{this.props.children}</h1>
        )
    }
}

SDHeading.propTypes = {
    style: PropTypes.object
}

SDHeading.defaultProps = {
    style: {}
}
