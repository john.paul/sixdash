import React, { Component, Children, cloneElement } from 'react'
import PropTypes from 'prop-types'

export default class SDHero extends Component {
    render() {
        const { children, headingStyles, callToActionStyles } = this.props

        let styles = {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
            backgroundColor: '#31303E',
            color: '#FFF',
            paddingTop: 50,
            paddingBottom: 30
        }

        styles = Object.assign(styles, this.props.style)

        return (
            <div style={styles}>
                {Children.map(children, (c, i) => {
                    let nStyle = {}
                    switch (c.type.name) {
                        case 'SDHeading':
                            nStyle = Object.assign(c.props.styles, headingStyles)
                            break;
                        case 'SDCallToAction':
                            nStyle  = Object.assign(c.props.styles, callToActionStyles)
                            break;
                        default:
                            nStyle = c.props.styles

                    }
                    return cloneElement(c, {
                        styles: nStyle
                    })
                }
                )}
            </div>
        )
    }
}

SDHero.propTypes = {
    style: PropTypes.object,
    headingStyles: PropTypes.object,
    callToActionStyles: PropTypes.object,
    children: PropTypes.arrayOf(PropTypes.element)
}

SDHero.defaultProps = {
    style: {},
    headingStyles: {},
    callToActionStyles: {}
}

