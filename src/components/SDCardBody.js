import React, { Component } from 'react'
import PropTypes from 'prop-types'
import SDText from './SDText'

class SDCardBody extends Component {
    render() {
        const { color, width, height, children } = this.props
        let styles = {
            background: '#F6F6F6',
            paddingTop: 10,
            paddingBottom: 10,
            paddingLeft: 10,
            paddingRight: 10,
            borderRadius: '0 0 5px 5px'
        }
        styles = Object.assign(styles, this.props.style)
        if (width) {
            styles = Object.assign(styles, {width})
        }
        if (height) {
            styles = Object.assign(styles, {height})
        }
        return (
            <div style={styles}>
                {children}
            </div>
        )
    }
}

export default SDCardBody

SDCardBody.propTypes = {
    style: PropTypes.object,
    children: PropTypes.oneOfType([
        PropTypes.instanceOf(SDText),
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element),
        PropTypes.string]),
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

SDCardBody.defaultProps = {
    style: {}
}
