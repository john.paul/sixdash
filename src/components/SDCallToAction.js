import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class SDCallToAction extends Component {
    render() {
        let styles = {}

        styles = Object.assign(styles, this.props.style)
        return (
            <div style={styles}>
                {this.props.children}
            </div>
        )
    }
}

SDCallToAction.propTypes = {
    style: PropTypes.object
}

SDCallToAction.defaultProps = {
    style: {}
}
