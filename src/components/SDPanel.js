import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Radium from 'radium'

const tablet = '@media screen and (max-width: 959px)'
const mobile = '@media screen and (max-width: 690px)'

class SDPanel extends Component {
    render() {
        let styles = {
            display: 'flex',
            width: 960,
            marginLeft: 'auto',
            marginRight: 'auto',
            justifyContent: 'space-around',
            [tablet]: {
                width: 680
            },
            [mobile]: {
                width: 320,
                flexDirection: 'column'
            }
        }

        styles = Object.assign(styles, this.props.style)
        return (
            <div style={styles}>
                {this.props.children}
            </div>
        )
    }
}

export default Radium(SDPanel)

SDPanel.propTypes = {
    style: PropTypes.object,
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.object])
}

SDPanel.defaultProps = {
    style: {}
}
