import React, { Component } from 'react'
import PropTypes from 'prop-types'

class SDCardHeader extends Component {
    render() {
        const { color, width, height, children } = this.props
        let styles = {
            marginTop: 10,
            paddingTop: 5,
            paddingBottom: 5,
            paddingLeft: 10,
            paddingRight: 10,
            borderRadius: '5px 5px 0 0'
        }
        if (color) {
            styles = Object.assign(styles, {color: '#FFF', background: color})
        }
        if (width) {
            styles = Object.assign(styles, {width})
        }
        if (height) {
            styles = Object.assign(styles, {height})
        }
        styles = Object.assign(styles, this.props.style)
        return (
            <div style={styles}>
                {children}
            </div>
        )
    }
}

export default SDCardHeader

SDCardHeader.propTypes = {
    style: PropTypes.object,
    color: PropTypes.string,
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
}

SDCardHeader.defaultProps = {
    style: {}
}
