import React,{ Component } from 'react'
import PropTypes from 'prop-types'

export default class SDText extends Component {
    render() {
        const { textColor, children } = this.props
        let styles = {
            fontWeight: 400,
            fontSize: 14,
            marginTop: 0,
            marginButton: 0
        }
        if (textColor) {
            styles = Object.assign(styles, {color: textColor})
        }

        styles = Object.assign(styles, this.props.style)
        return (
            <p style={styles}>{children}</p>
        )
    }
}

SDText.propTypes = {
    style: PropTypes.object,
    textColor: PropTypes.string
}

SDText.defaultProps = {
    style: {}
}
