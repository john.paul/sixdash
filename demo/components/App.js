import React, { Component } from 'react'
import {
    SixDash, SDHero, SDHeading,
    SDSubHeading, SDText, SDCallToAction,
    SDPanel, SDPanelSection, SDPanelHeading,
    SDCard, SDCardHeader, SDCardBody,
    COLORS
} from '../../build'
import TwoLevelPie, { SimplePieChart } from '../mock/PieChart2'
import moment from 'moment'

export default class App extends Component {
    render() {
        return (
            <SixDash>
                <SDHero>
                    <SDHeading>Welcome!</SDHeading>
                    <SDSubHeading style={{color: '#FFF'}}>Today: {moment().format('LL')}</SDSubHeading>
                    <SDCallToAction>
                        <button>Goto Schedule</button>
                    </SDCallToAction>
                </SDHero>

                <SDPanel>
                    <SDPanelSection heading={
                        <SDPanelHeading>Today</SDPanelHeading>
                        }>
                        <SDCard width={300}>
                            <SDCardHeader color={COLORS[0]}>Header</SDCardHeader>
                            <SDCardBody>
                                <SDText>Body</SDText>
                            </SDCardBody>
                        </SDCard>
                    </SDPanelSection>
                    <SDPanelSection heading={
                        <SDPanelHeading>This Week</SDPanelHeading>
                        }>
                        <SDCard width={300}>
                            <SDCardHeader color={COLORS[4]}>Header</SDCardHeader>
                            <SDCardBody>
                                <SDText>Body</SDText>
                            </SDCardBody>
                        </SDCard>
                    </SDPanelSection>
                </SDPanel>

                <SDPanel>
                    <SDPanelSection heading={
                        <SDPanelHeading>Today</SDPanelHeading>
                    } position="bottom">
                        <TwoLevelPie />
                    </SDPanelSection>
                    <SDPanelSection heading={
                        <SDPanelHeading>This Week</SDPanelHeading>
                    } position="bottom">
                        <TwoLevelPie />
                    </SDPanelSection>
                </SDPanel>
            </SixDash>
        )
    }
}
