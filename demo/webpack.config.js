var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');
var precss = require('precss');
var autoprefixer = require('autoprefixer');

var isProd = process.env.NODE_ENV === 'production';

module.exports = {
    entry: path.join(__dirname, 'app.js'),

    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].js'
    },

    devtool: 'hidden-source-map',

    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        stats: "errors-only",
        hot: true,
        // open: true
    },

    module: {
        rules: [
            {
                test: /\.scss$/,
                use: isProd ? ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader?sourceMap', 'sass-loader?sourceMap', 'postcss-loader'],
                    publicPath: path.join(__dirname, 'dist')
                }) : ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap', 'postcss-loader']
            },
            {
                test: /\.((woff2?|svg)(\?v=[0-9]\.[0-9]\.[0-9]))|(woff2?|svg|jpe?g|png|gif|ico)$/,
                use: 'url-loader?limit=10000'
            },
            {
                test: /\.((ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9]))|(ttf|eot)$/,
                use: 'file-loader?name=[name].[ext]&publicPath=fonts/&outputPath=fonts/'
            },
            {
                test: /\.js$/,
                exclude: [/node_modules/, /build/],
                use: [
                    {
                        loader: 'babel-loader'
                    }
                ]
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: 'Six Dash',
            template: path.join(__dirname, 'index.tmpl.html'),
            hash: true
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer({browsers: ["last 2 versions", "last 3 iOS versions"]}),
                    precss
                ]
            }
        })
    ]
};
