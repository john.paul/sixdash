import React from 'react'
import PropTypes from 'prop-types'
import Recharts, { PieChart, Pie } from 'recharts'

export default class PieChartMock extends React.Component {
    render() {
        const { data, color } = this.props
        let styles = {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
        return (
            <div style={styles}>
                <PieChart width={600} height={400}>
                    <Pie data={data} cx="50%" cy="50%" innerRadius={60} outerRadius={80} fill={color} label />
                </PieChart>
            </div>
        )
    }
}

PieChartMock.propTypes = {
    data: PropTypes.array,
    color: PropTypes.string
}

PieChartMock.defaultProps = {
    data: [{value: 60}, {value: 40}],
    color: '#82CA9D'
}
