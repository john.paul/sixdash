import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Recharts, { PieChart, Pie, Cell, Sector, Tooltip, Legend } from 'recharts';

const data = [{name: 'LibreLink Prototype', code: '(17-ADC-PT)', value: 20}, {name: 'Paypal Prototype', code: '(17-PAL-PT)', value: 40}, {name: 'Beer Pong', code: '(17-BP-MOB)', value: 40}]
const COLORS = ['#ff4b42', '#0088FE', '#00C49F', '#1daae8', '#FFBB28', '#FF8042', '#00c143', '#B7E65A', '#EBEBEB', '#212121'];

const renderActiveShape = (props) => {
  const RADIAN = Math.PI / 180;
  const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
    fill, payload, percent, value } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const textAnchor = cos >= 0 ? 'start' : 'end';

  return (
    <g>
        <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill} style={{fontSize: 12}}>{`${payload.code}`}</text>
        <Sector
            cx={cx}
            cy={cy}
            innerRadius={innerRadius}
            outerRadius={outerRadius}
            startAngle={startAngle}
            endAngle={endAngle}
            fill={fill}
        />
        <Sector
            cx={cx}
            cy={cy}
            startAngle={startAngle}
            endAngle={endAngle}
            innerRadius={outerRadius + 6}
            outerRadius={outerRadius + 10}
            fill={fill}
        />
    </g>
  );
};

const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
  const RADIAN = Math.PI / 180;
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x  = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy  + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
        {`${(percent*100)}%`}
    </text>
  );
};

class TwoLevelPieChart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeIndex: 0
        }
    }

    onPieEnter(data, index) {
        this.setState({
            activeIndex: index
        })
    }

    render() {
        return (
            <PieChart width={320} height={300} style={{margin: 'auto'}} onMouseEnter={this.onPieEnter.bind(this)}>
                <Pie
                    activeIndex={this.state.activeIndex}
                    activeShape={renderActiveShape} 
                    data={data}
                    cx={160}
                    cy={150}
                    innerRadius={50}
                    paddingAngle={5}
                    outerRadius={80}>
                    {
                        data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index]}/>
                            ))
                    }
                </Pie>
                <Legend />
            </PieChart>
        )
    }
}

class SimplePieChart extends Component {
    render() {
        return (
            <PieChart width={320} height={300} style={{margin: 'auto'}} onMouseEnter={this.onPieEnter}>
                <Pie
                    data={data}
                    cx={160}
                    cy={150}
                    labelLine={false}
                    label={renderCustomizedLabel}
                    outerRadius={80}
                >
                    {
                        data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index]}/>)
                    }
                </Pie>
                <Legend />
            </PieChart>
        )
    }
}

export { SimplePieChart };
export default TwoLevelPieChart;
