# Plugable dashboard

This project uses UI View Framework ReactJS.

```javascript
<SixDash style={{display: 'inline'}}>
    <SDHero>
        <SDHeading>Welcome!</SDHeading>
        <SDSubHeading style={{color: '#FFF'}}>Sub title here</SDSubHeading>
        <SDCallToAction>
            <button>Login</button>
        </SDCallToAction>
    </SDHero>

    <SDPanel>
        <SDPanelSection heading={
            <SDPanelHeading>Today</SDPanelHeading>
            }>
            <SDCard width={300}>
                <SDCardHeader color={COLORS[0]}>Header</SDCardHeader>
                <SDCardBody>
                    <SDText>Body</SDText>
                </SDCardBody>
            </SDCard>
        </SDPanelSection>
        <SDPanelSection heading={
            <SDPanelHeading>This Week</SDPanelHeading>
            }>
            <SDCard width={300}>
                <SDCardHeader color={COLORS[4]}>Header</SDCardHeader>
                <SDCardBody>
                    <SDText>Body</SDText>
                </SDCardBody>
            </SDCard>
        </SDPanelSection>
    </SDPanel>

    <SDPanel>
        <SDPanelSection heading={
            <SDPanelHeading>Today</SDPanelHeading>
        } position="bottom">
            <TwoLevelPie />
        </SDPanelSection>
        <SDPanelSection heading={
            <SDPanelHeading>This Week</SDPanelHeading>
        } position="bottom">
            <TwoLevelPie />
        </SDPanelSection>
    </SDPanel>
</SiDash>
```

See demo for full example.


## SixDash
The main container

### Properties
style - Object | This will apply to the container

## SDHero

### Properties
style - Object | This will apply to the container

#### SDHero Child
* SDHeading
* SDSubHeading
* SDCallToAction

## SDPanel
Main container for dashboard component

## SDPanelSection
Dashboard component wrapper

- Properties
    - SDPanelHeading | Text

#### SDPanelSection Child
* SDCard

## SDCard
Container with header and body section

#### SDCard Child
* SDCardHeader
* SDCardBody

## SDText
Pre styled paragraph tag
